import SidebarItem from './SidebarItem';
import SidebarLogo from './SidebarLogo';
import SidebarTypeButton from './SidebarTypeButton';
import useCurrentUser from '@/hooks/useCurrentUser';
import { signOut } from 'next-auth/react';
import { AiOutlineBell, AiOutlineUser } from 'react-icons/ai';
import { BiLogOut } from 'react-icons/bi';
import { FaHouseUser } from 'react-icons/fa';

const Sidebar = () => {
	const { data: currentUser } = useCurrentUser();

	const items = [
		{
			icon: FaHouseUser,
			label: 'Home',
			href: '/',
		},
		{
			icon: AiOutlineBell,
			label: 'Notifications',
			href: '/notifications',
			auth: true,
			alert: currentUser?.hasNotification,
		},
		{
			icon: AiOutlineUser,
			label: 'Profile',
			href: `/users/${currentUser?.id}`,
			auth: true,
		},
	];

	return (
		<div className="col-span-1 h-full pr-4 md:pr-6">
			<div className="flex flex-col items-end">
				<div className="space-y-2 lg:w-[230px]">
					<SidebarLogo />
					{items.map((item) => (
						<SidebarItem
							key={item.href}
							alert={item.alert}
							auth={item.auth}
							href={item.href}
							icon={item.icon}
							label={item.label}
						/>
					))}
					{currentUser && (
						<SidebarItem
							onClick={() => signOut()}
							icon={BiLogOut}
							label="Logout"
						/>
					)}
					<SidebarTypeButton />
				</div>
			</div>
		</div>
	);
};

export default Sidebar;
