import { ChangeEvent } from 'react';

interface InputProps {
	placeholder?: string;
	value?: string;
	type?: string;
	disabled?: boolean;
	onChange: (event: ChangeEvent<HTMLInputElement>) => void;
	label?: string;
	pattern?: string;
	isValid?: boolean;
}

const Input: React.FC<InputProps> = ({
	placeholder,
	value,
	type = 'text',
	onChange,
	disabled,
	label,
	pattern,
	isValid = true,
}) => {
	const validateInput = isValid ? '' : 'border-rose-500';

	return (
		<div className="w-full">
			{label && (
				<p className="text-xl text-white font-semibold mb-2">{label}</p>
			)}
			<input
				disabled={disabled}
				onChange={onChange}
				value={value}
				placeholder={placeholder}
				type={type}
				pattern={pattern}
				className={`w-full
          p-4
          text-lg
          bg-black
          border-2
          border-neutral-800
          rounded-md
          outline-none
          text-white
          focus:border-indigo-500
          focus:border-2
          transition
          disabled:bg-neutral-900
          disabled:opacity-70
          disabled:cursor-not-allowed
          ${validateInput}
          `}
			/>
		</div>
	);
};

export default Input;
