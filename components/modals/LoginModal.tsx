import Input from '../Input';
import Modal from '../Modal';
import useLoginModal from '@/hooks/useLoginModal';
import useRegisterModal from '@/hooks/useRegisterModal';
import {
	emailRegex,
	emailRegexPattern,
} from '@/shared/consts/emailRegexPattern';
import { signIn } from 'next-auth/react';
import { ChangeEvent, useCallback, useState } from 'react';
import { toast } from 'react-hot-toast';

const LoginModal = () => {
	const [email, setEmail] = useState('');
	const loginModal = useLoginModal();
	const [password, setPassword] = useState('');
	const registerModal = useRegisterModal();

	const [isLoading, setIsLoading] = useState(false);
	const [emailTouched, setEmailTouched] = useState(false);
	const [passwordTouched, setPasswordTouched] = useState(false);

	const isEmailValid = emailRegex.test(email);
	const shouldShowEmailError = emailTouched && !isEmailValid;

	const isPasswordValid = password.length >= 6;
	const shouldShowPasswordError = passwordTouched && !isPasswordValid;

	const handleDisabled = !isEmailValid || !isPasswordValid;

	const onSubmit = useCallback(async () => {
		try {
			setIsLoading(true);

			await signIn('credentials', {
				email,
				password,
			});

			toast.success('Logged in');

			loginModal.onClose();
		} catch (error) {
			toast.error('Something went wrong');
		} finally {
			setIsLoading(false);
		}
	}, [email, password, loginModal]);

	const onToggle = useCallback(() => {
		loginModal.onClose();
		registerModal.onOpen();
	}, [loginModal, registerModal]);

	const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
		const value = e.target.value;
		setEmail(value);
		setEmailTouched(true);
	};

	const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
		const value = e.target.value;
		setPassword(value);
		setPasswordTouched(true);
	};

	const bodyContent = (
		<div className="flex flex-col gap-4">
			<Input
				placeholder="Email"
				onChange={handleEmailChange}
				value={email}
				disabled={isLoading}
				isValid={!shouldShowEmailError}
				pattern={emailRegexPattern}
			/>
			<p
				className={`text-center
			text-rose-500 mt-2 ${!shouldShowEmailError ? 'invisible' : ''}
			`}
			>
				example.@mail.com
			</p>
			<Input
				placeholder="Password"
				type="password"
				onChange={handlePasswordChange}
				value={password}
				disabled={isLoading}
				isValid={!shouldShowPasswordError}
			/>
			<p
				className={`text-center
			text-rose-500 mt-2 ${!shouldShowPasswordError ? 'invisible' : ''}
			`}
			>
				Input must be 6 characters long
			</p>
		</div>
	);

	const footerContent = (
		<div className="text-neutral-400 flex items-center justify-center gap-2 text-center mt-4">
			<p>First time using app?</p>
			<span
				onClick={onToggle}
				className="
            text-white
            cursor-pointer
            hover:underline
          "
			>
				Create an account
			</span>
		</div>
	);

	return (
		<Modal
			disabled={handleDisabled}
			isOpen={loginModal.isOpen}
			title="Login"
			actionLabel="Sign in"
			onClose={loginModal.onClose}
			onSubmit={onSubmit}
			body={bodyContent}
			footer={footerContent}
			email={email}
			password={password}
		/>
	);
};

export default LoginModal;
