import Input from '../Input';
import Modal from '../Modal';
import useLoginModal from '@/hooks/useLoginModal';
import useRegisterModal from '@/hooks/useRegisterModal';
import {
	emailRegex,
	emailRegexPattern,
} from '@/shared/consts/emailRegexPattern';
import axios from 'axios';
import { signIn } from 'next-auth/react';
import { ChangeEvent, useCallback, useState } from 'react';
import { toast } from 'react-hot-toast';

const RegisterModal = () => {
	const loginModal = useLoginModal();
	const registerModal = useRegisterModal();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [username, setUsername] = useState('');
	const [name, setName] = useState('');
	const [emailTouched, setEmailTouched] = useState(false);
	const [passwordTouched, setPasswordTouched] = useState(false);

	const [isLoading, setIsLoading] = useState(false);

	const isEmailValid = emailRegex.test(email);
	const shouldShowEmailError = emailTouched && !isEmailValid;

	const isPasswordValid = password.length >= 6;
	const shouldShowPasswordError = passwordTouched && !isPasswordValid;

	const handleDisabled = !isEmailValid || !isPasswordValid;

	const onToggle = useCallback(() => {
		if (isLoading) {
			return;
		}

		registerModal.onClose();
		loginModal.onOpen();
	}, [loginModal, registerModal, isLoading]);

	const onSubmit = useCallback(async () => {
		try {
			setIsLoading(true);

			await axios.post('/api/register', {
				email,
				password,
				username,
				name,
			});

			setIsLoading(false);

			toast.success('Account created.');

			signIn('credentials', {
				email,
				password,
			});

			registerModal.onClose();
		} catch (error) {
			toast.error('Something went wrong');
		} finally {
			setIsLoading(false);
		}
	}, [email, password, registerModal, username, name]);

	const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
		const value = e.target.value;
		setEmail(value);
		setEmailTouched(true);
	};

	const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
		const value = e.target.value;
		setPassword(value);
		setPasswordTouched(true);
	};

	const bodyContent = (
		<div className="flex flex-col gap-4">
			<Input
				disabled={isLoading}
				placeholder="Email"
				value={email}
				onChange={handleEmailChange}
				isValid={!shouldShowEmailError}
				pattern={emailRegexPattern}
			/>
			<p
				className={`text-center
			text-rose-500 mt-2 ${!shouldShowEmailError ? 'invisible' : ''}
			`}
			>
				example.@mail.com
			</p>
			<Input
				disabled={isLoading}
				placeholder="Name"
				value={name}
				onChange={(e) => setName(e.target.value)}
			/>
			<Input
				disabled={isLoading}
				placeholder="Username"
				value={username}
				onChange={(e) => setUsername(e.target.value)}
			/>
			<Input
				disabled={isLoading}
				placeholder="Password"
				type="password"
				value={password}
				onChange={handlePasswordChange}
				isValid={!shouldShowPasswordError}
			/>
			<p
				className={`text-center
			text-rose-500 mt-2 ${!shouldShowPasswordError ? 'invisible' : ''}
			`}
			>
				Input must be 6 characters long
			</p>
		</div>
	);

	const footerContent = (
		<div className="text-neutral-400 text-center mt-4">
			<p>
				Already have an account?
				<span
					onClick={onToggle}
					className="
            text-white
            cursor-pointer
            hover:underline
          "
				>
					{' '}
					Sign in
				</span>
			</p>
		</div>
	);

	return (
		<Modal
			disabled={handleDisabled}
			isOpen={registerModal.isOpen}
			title="Create an account"
			actionLabel="Register"
			onClose={registerModal.onClose}
			onSubmit={onSubmit}
			body={bodyContent}
			footer={footerContent}
		/>
	);
};

export default RegisterModal;
