import Form from '@/components/Form';
import Header from '@/components/Header';
import PostFeed from '@/components/posts/PostFeed';
import { Metadata } from 'next';

export default function Home() {
	return (
		<>
			<title>VK+</title>
			<Header label="Home" />
			<Form placeholder="What's new?" />
			<PostFeed />
		</>
	);
}
