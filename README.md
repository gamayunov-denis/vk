# <a href="https://vk-plus-git-readme-gamaunov.vercel.app" >VK+</a>

<h3>Links:</h3>


<a href="https://nextjs.org/" > <img alt="nextjs" src="https://img.shields.io/badge/Next.JS-black?style=for-the-badge&logo=nextdotjs&logoColor=#000000"/></a>

<a href="https://tailwindcss.com/" > <img alt="tailwindcss" src="https://img.shields.io/badge/Tailwind CSS-06B6D4?style=for-the-badge&logo=Tailwind CSS&logoColor=008FC7"/></a>

<a href="https://react.dev/" > <img alt="react" src="https://img.shields.io/badge/React-2C3454?style=for-the-badge&logo=React&logoColor=61DAFB"/> </a>

<a href="https://www.typescriptlang.org/" > <img alt="typescript" src="https://img.shields.io/badge/TypeScript-3178C6?style=for-the-badge&logo=TypeScript&logoColor=008FC7"/></a>

<a href="https://www.mongodb.com/" > <img alt="mongodb" src="https://img.shields.io/badge/mongodb-47A248?style=for-the-badge&logo=mongodb&logoColor=008FC7"/></a>

<a href="https://www.prisma.io/" > <img alt="prisma" src="https://img.shields.io/badge/prisma-2D3748?style=for-the-badge&logo=prisma&logoColor=008FC7"/></a>


![design](./shared/images/design/1.png)
![design](./shared/images/design/2.png)


